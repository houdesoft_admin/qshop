<?php

/**
 * +----------------------------------------------------------------------
 * | 润憬商城系统 [ 高性价比的通用商城系统 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2022~2023 https: *www.honc.fun All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
 * +----------------------------------------------------------------------
 * | Author: 润憬科技 Hon(陈烁临) <2275604210@qq.com>
 * +----------------------------------------------------------------------
 */

declare(strict_types=1);

namespace cores;

use cores\traits\RequestTrait;
use support\App as SupportApp;
use support\Response;
use think\Validate;
use think\exception\ValidateException;
use Webman\App;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    use RequestTrait;
    /**
     * Request实例
     * @var \support\Request
     */
    protected \support\Request  $request;

    /**
     * 应用实例
     * @var \Webman\App
     */
    protected App $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected bool $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected array $middleware = [];

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct()
    {
        // $this->app = App::class;
        $this->request = request();

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {
    }

    /**
     * 验证数据
     * @access protected
     * @param array $data 数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array $message 提示信息
     * @param bool $batch 是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        return true;
        //没用方法
        // if (is_array($validate)) {
        //     $v = new Validate();
        //     $v->rule($validate);
        // } else {
        //     if (strpos($validate, '.')) {
        //         // 支持场景
        //         [$validate, $scene] = explode('.', $validate);
        //     }
        //     $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
        //     $v = new $class();
        //     if (!empty($scene)) {
        //         $v->scene($scene);
        //     }
        // }

        // $v->message($message);

        // // 是否批量验证
        // if ($batch || $this->batchValidate) {
        //     $v->batch(true);
        // }

        // return $v->failException(true)->check($data);
    }

    /**
     * 返回封装后的 API 数据到客户端
     * @param int|null $status
     * @param string $message
     * @param array $data
     * @return Response
     */
    protected final function renderJson(int $status = null, string $message = '', array $data = []): Response
    {
        return json(compact('status', 'message', 'data'));
    }

    /**
     * 返回操作成功json
     * @param array|string $data
     * @param string $message
     * @return Response
     */
    protected final function renderSuccess($data = [], string $message = 'success'): Response
    {
        if (is_string($data)) {
            $message = $data;
            $data = [];
        }
        return $this->renderJson(config('status.success'), $message, $data);
    }

    /**
     * 返回操作失败json
     * @param string $message
     * @param array $data
     * @return Response
     */
    protected final function renderError(string $message = 'error', array $data = []): Response
    {
        return $this->renderJson(config('status.error'), $message, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param null $key
     * @param bool $filter
     * @return mixed
     */
    protected final function postData($key = null, bool $filter = false)
    {
        // return $this->all(empty($key) ? '' : "{$key}/a", null, $filter ? '' : null);
        return $this->param(empty($key) ? '' : $key, null, $filter ? '' : null);
    }

    /**
     * 获取post数据 (数组)
     * @param string|null $key
     * @param bool $filter
     * @return mixed
     */
    protected final function postForm(?string $key = 'form', bool $filter = true)
    {
        return $this->postData(empty($key) ? 'form' : $key, $filter);
    }
}
