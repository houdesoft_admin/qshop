<?php

return [
    'default' => 'mysql',
    'connections' => [
        'mysql' => [
            // 数据库类型
            'type' => 'mysql',
            // 服务器地址
            'hostname' => env("MYSQL_DB_HOST", '127.0.0.1'),
            // 数据库名
            'database' => env("MYSQL_DB_NAME", 'qshop_db'),
            // 数据库用户名
            'username' => env("MYSQL_DB_USER", 'root'),
            // 数据库密码
            'password' => env("MYSQL_DB_PASSWORD", 'root'),
            // 数据库连接端口
            'hostport' => env("MYSQL_DB_PORT", '3306'),
            // 数据库连接参数
            'params' => [
                // 连接超时3秒
                \PDO::ATTR_TIMEOUT => 3,
            ],
            // 数据库编码默认采用utf8
            'charset' => 'utf8',
            // 数据库表前缀
            'prefix' => env('MYSQL_DB_PREFIX', 'qshop_'),
            // 断线重连
            'break_reconnect' => true,
            // 关闭SQL监听日志
            'trigger_sql' => false,
            // 自定义分页类
            'bootstrap' =>  ''
        ],
    ],
];
